<?php get_header(); ?>

<div class="page_header">
  <div class="bg"></div>

  <div class="text_container">
    <div class="text">
      <h1>404 - Page Not Found</h1>
    </div>
  </div>
</div>

<main id="primary" class="site-main">
  <div class="archive_container">
    <p><strong>The requested page "<?php echo esc_attr($GLOBALS['wp']->request); ?>" could not be found.</strong></p>

	<?php
      $query_parameter = trim(preg_replace('/[^\w\d]/', ' ', $GLOBALS['wp']->request));
      $search = new WP_Query(array(
        's' => $query_parameter,
        'order_by' => 'levenshtein("'.$query_parameter.'", post_name)',
        'posts_per_page' => '10',
      ));

      if ($search->have_posts()) {
	?>
        <p>Were you looking for one of the following;</p>
        <ul>
          <?php while ($search->have_posts()) { ?>
            <?php $search->the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
          <?php } ?>
        </ul>
    <?php
	  }
	?>
  </div>
</main>

<?php get_footer(); ?>