<?php
  get_header();

  if(get_field("background_image", get_the_id()) != '') {
    $image = get_field("background_image", get_the_id());
  } else {
    $image = get_the_post_thumbnail_url(get_the_ID(), 'full');
  }

  if(get_field("title", get_the_id()) != '') {
    $title = get_field("title", get_the_id());
  } else {
    $title = get_the_title();
  }

  if(get_field("description", get_the_id()) != '') {
    $description = get_field("description", get_the_id());
  } else {
    $description = get_the_excerpt();
  }
?>

<?php
  if(!is_front_page() && !is_home()) {
?>
    <div class="page_header">
      <div class="wrapper">
        <div class="text">
          <h1 style="font-weight: 700;"><?php echo $title; ?></h1>
          <?php if(get_field("hide_excerpt", get_the_id() == 1)) { echo $description; } ?>
        </div>

        <div class="bg">
          <div class="image" style="background-image: url('<?php echo $image; ?>');"></div>
        </div>
      </div>
    </div>
<?php
  }
?>

<main id="primary" class="site-main">
  <?php
    while(have_posts()) {
      the_post();

      the_content();
    }
  ?>
</main>

<?php
  if(get_the_ID() == '1568') {
?>
    <!-- AddToAny BEGIN -->
    <div class="a2a_kit a2a_kit_size_32 a2a_floating_style a2a_vertical_style" data-a2a-url="<?php echo get_permalink(); ?>" data-a2a-title="Vacancies - Central Pattern Making UK" style="left: 0px; top: 50%; transform: translateY(-50%); background-color: rgba(255, 255, 255, 0.8);">
      <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
      <a class="a2a_button_facebook"></a>
      <a class="a2a_button_linkedin"></a>
      <a class="a2a_button_twitter"></a>
      <a class="a2a_button_email"></a>
    </div>

    <script>
      var a2a_config = a2a_config || {};
      a2a_config.onclick = 1;
    </script>

    <script async src="https://static.addtoany.com/menu/page.js"></script>
    <!-- AddToAny END -->
<?php
  }
?>

<?php get_footer(); ?>
