<?php
  get_header();

  $featured_image = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

<div class="page_header">
  <?php
    if($featured_image != '') {
      echo '<div class="bg" style="background-image: url('. $featured_image .')"></div>';
    }

    else {
      echo '<div class="bg"></div>';
    }
  ?>

  <div class="text_container">
    <div class="text">
      <h1><?php echo get_the_title(); ?></h1>
    </div>
  </div>
</div>

<main id="primary" class="site-main">
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  </article>
</main>

<?php get_footer(); ?>
