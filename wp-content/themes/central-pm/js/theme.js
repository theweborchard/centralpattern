jQuery(document).ready(function($) {
  // Add class to body on scroll
  $('.screenheight').height($(window).height());
  $(window).scroll(function() {
    if ($(window).scrollTop() > 10) {
      $('body').addClass('scroll');
    } else {
      $('body').removeClass('scroll');
    }
  });

  // Add class to body when mobile menu open
  $('.mobile_menu').click(function(event) {
    $('body').toggleClass('menuopen');
    event.preventDefault();
  });
});