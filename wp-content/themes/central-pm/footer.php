<?php
  if(!is_front_page() && !is_home()) {
?>
    <div class="breadcrumb">
      <div class="wrapper">
        <?php echo do_shortcode('[wpseo_breadcrumb]'); ?>
      </div>
    </div>
<?php
  }
?>

<footer id="colophon" class="site-footer">
  <div class="site-info">
    <?php dynamic_sidebar('footer-1'); ?>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
