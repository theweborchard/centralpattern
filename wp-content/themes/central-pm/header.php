<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<header id="masthead" class="site-header">
  <div class="header_widgets">
    <?php dynamic_sidebar('header-1'); ?>
  </div>

  <a href="#" class="mobile_menu">
    <span></span>
    <span></span>
    <span></span>
  </a>
</header>