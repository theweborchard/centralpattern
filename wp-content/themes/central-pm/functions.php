<?php
  /* Add theme / post support and filters for the following */
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');
  add_theme_support('customize-selective-refresh-widgets');
  add_theme_support('post-thumbnails');
  add_theme_support('disable-custom-font-sizes');
  add_theme_support('title-tag');
  add_theme_support('disable-comments');

  add_post_type_support('page', 'excerpt');

  add_filter('use_block_editor_for_post_type', '__return_false', 100);
  add_filter('gutenberg_can_edit_post_type', '__return_false', 100);
  add_filter('gutenberg_use_widgets_block_editor', '__return_false', 100);
  
  /* Site origin specific filters */
  add_filter('so_panels_show_classic_admin_notice', '__return_false');
  add_filter('siteorigin_layout_block_show_add_button', '__return_false');

  add_filter('enable_post_by_email_configuration', '__return_false', 100);

  /* Register menu locations */
  register_nav_menus(
	array(
	  'menu-1' => esc_html__('Primary', 'theme'),
	)
  );

  add_theme_support(
	'html5',
	array(
	  'search-form',
	  'gallery',
	  'caption',
	)
  );

  /* Add custom classes to site origin elements when background-theme is used (filter: siteorigin_panels_general_style_fields) */
  add_filter('siteorigin_panels_general_style_attributes', function($attr, $style) {
	if(!empty($style['background_theme'])) {
	  if(!isset($attr['class'])) {
		$attr['class'] = [];
	  }

	  $attr['class'][] = $style['background_theme'];
	  $attr['class'][] = 'bg';
	}

	return $attr;
  }, 10, 2);

  /* Brand palette, manually sync with styles.scss */
  $brand_palette = [
    'primary'     =>        ['name' => 'Primary'],
    'secondary'   =>        ['name' => 'Secondary'],
    'white-text'   =>       ['name' => 'White Text'],
  ];

  /* Additional image sizes */
  add_image_size('post-thumbnail', 400, 300, true);
  add_image_size('call-to-action', 1900, 350, true);
  add_image_size('hp-rectangle', 585, 230, true);
  add_image_size('hp-square', 375, 235, true);
  add_image_size('gallery-thumb', 1000, 650, true);

  /* Register widget area */
  function theme_widgets_init() {
    register_sidebar(
      array(
        'name'          => esc_html__('Header', 'theme'),
        'id'            => 'header-1',
        'description'   => esc_html__('Add widgets here.', 'theme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
      )
    );

    register_sidebar(
      array(
        'name'          => esc_html__('Footer Main', 'theme'),
        'id'            => 'footer-1',
        'description'   => esc_html__('Add widgets here.', 'theme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<p class="widget-title">',
        'after_title'   => '</p>',
      )
    );
  }

  add_action('widgets_init', 'theme_widgets_init');

  /* Enqueue scripts and styles */
  function theme_scripts() {
    wp_enqueue_style('theme', get_stylesheet_directory_uri() . '/style.css', '1.0.0', true);

    wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/jquery-3.6.0.min.js', '1.0.0', true);
    wp_enqueue_script('theme', get_stylesheet_directory_uri() . '/js/theme.js', '1.0.0', true);
    wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/2e5f9ded20.js', '1.0.0', true);
  }

  add_action('wp_enqueue_scripts', 'theme_scripts');

  /* Disable the emojis */
  function disable_emojis() {
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
    add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
  }

  add_action('init', 'disable_emojis');

  /* Filter function used to remove the tinymce emoji plugin */
  function disable_emojis_tinymce($plugins) {
    if(is_array( $plugins)) {
      return array_diff($plugins, array('wpemoji'));
    }

    else {
      return array();
    }
  }

  /* Remove emoji CDN hostname from DNS prefetching hints */
  function disable_emojis_remove_dns_prefetch($urls, $relation_type) {
    if('dns-prefetch' == $relation_type) {
      $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');
      $urls = array_diff($urls, array( $emoji_svg_url));
    }

    return $urls;
  }

  /* Remove the default styling for when admin bar is present */
  function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
  }

  add_action('get_header', 'remove_admin_login_header');

  /* Remove site origin button themes */
  add_action('init', function() {
    function theme_sow_button_form($form_options) {
      unset($form_options['button_icon'], $form_options['design']['fields']['theme'], $form_options['design']['fields']['color'], $form_options['design']['fields']['button_color'], $form_options['design']['fields']['text_color'], $form_options['design']['fields']['hover'], $form_options['design']['fields']['hover_background_color'], $form_options['design']['fields']['hover_text_color'], $form_options['design']['fields']['font'], $form_options['design']['fields']['font_size'], $form_options['design']['fields']['rounding'], $form_options['design']['fields']['padding'],);
      return $form_options;
    }

    add_filter('siteorigin_widgets_form_options_sow-button', 'theme_sow_button_form');

    function theme_sow_button_widget($instance) {
      if(strlen($instance['attributes']['classes'])) {
        if(preg_match('/\bbutton\b/i', $instance['attributes']['classes']) === false) {
          $instance['attributes']['classes'] .= ' button';
        }
      }

      else {
        $instance['attributes']['classes'] = 'button';
      }

      return $instance;
    }

    add_filter('siteorigin_widgets_instance_sow-button', 'theme_sow_button_widget');

    function theme_sow_button_classes($classes) {
      array_pop($classes);

      return $classes;
    }

      add_filter('siteorigin_widgets_wrapper_classes_sow-button', 'theme_sow_button_classes');
  }, 5);

  /* Add background theme options to site origin elements */
  add_action('admin_init', function() {
    add_filter('siteorigin_panels_general_style_fields', function($fields) {
      global $brand_palette;

      if(empty($brand_palette)) {
        $brand_palette = [];
      }

      $colors = array_combine(array_keys($brand_palette), array_column($brand_palette, 'name'));
      $colors = array_merge(['none' => 'None'], $colors);
      $keys = array_keys($colors);

      $fields['background_theme'] = [
        'name'    => 'Background theme',
        'type'    => 'select',
        'group'   => 'design',
        'default' => reset($keys),
        'options' => $colors,
      ];

      return $fields;
    }, 10, 1);
  }, PHP_INT_MAX - 100);

  /* Disables the block editor from managing widgets in the Gutenberg plugin */
  add_filter('gutenberg_use_widgets_block_editor', '__return_false');

  /* Disables the block editor from managing widgets */
  add_filter('use_widgets_block_editor', '__return_false');

  // Additional code to head
  add_action('wp_head', 'two_additional_head');

  function two_additional_head() {
?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53637475-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-53637475-1');
  </script>
<?php
}
?>