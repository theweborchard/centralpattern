<?php get_header(); ?>

<div class="page_header">
  <div class="bg"></div>

  <div class="text_container">
    <div class="text">
      <h1><?php echo get_the_archive_title(); ?></h1>
    </div>
  </div>
</div>

<main id="primary" class="site-main">
  <div class="archive_container">
    <?php
      while(have_posts()) {
        the_post();

        $id = get_the_id();
        $link = get_post_permalink();
        $featured_image = get_the_post_thumbnail_url(get_the_ID(), 'post-thumbnail');
        $title = get_the_title();
        $date = get_the_date();
        $excerpt = get_the_excerpt();
    ?>
        <a class="container" href="<?php echo $link; ?>" title="<?php echo $title; ?>">
          <div class="post" id="post-<?php echo $id; ?>">
            <img src="<?php echo $featured_image; ?>" />
            <p class="title" style="margin: 0;"><strong><?php echo $title; ?></strong></p>
            <p style="margin-top: 0;"><?php echo $date; ?></p>
            <p><?php echo $excerpt; ?></p>
          </div>
        </a>
    <?php
      }
    ?>
  </div>
</main>

<?php get_footer(); ?>
